FROM registry.gitlab.com/base-images/debian:12

ENV APACHE_LOCK_DIR /opt/apache2/var/lock/
ENV APACHE_LOG_DIR /opt/apache2/logs/
ENV APACHE_PID_FILE /opt/apache2/var/run/apache2.pid
ENV APACHE_RUN_DIR /opt/apache2/var/run/
ENV APACHE_VERSION 2.4.62-*

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
    apache2="${APACHE_VERSION}" \
    libapache2-mod-md="${APACHE_VERSION}" \
 && mkdir -p /opt/apache2/var/lock/ \
 && mkdir -p /opt/apache2/var/run/ \
 && mkdir -p /opt/apache2/logs/ \
 && apt-get autoremove \
 && apt-get clean \
 && rm -rf /tmp/* \
 && rm -rf /var/lib/apt/lists/*

RUN sed -i 's/^ErrorLog.*/ErrorLog "| \/bin\/cat"/;\
    s/APACHE_RUN_USER/RUN_USER/;\
    s/APACHE_RUN_GROUP/RUN_GROUP/' /etc/apache2/apache2.conf \
 && sed -i 's/^Listen 80/Listen 8080/;\
    s/Listen 443/Listen 8443/' /etc/apache2/ports.conf \
 && sed -i 's/^ServerTokens OS/ServerTokens Prod/;\
    s/^ServerSignature On/ServerSignature Off/' /etc/apache2/conf-enabled/security.conf \
 && sed -i 's/^<VirtualHost *:80/<VirtualHost *:8080/' /etc/apache2/sites-enabled/000-default.conf

CMD ["/usr/sbin/apache2", "-k", "start", "-DFOREGROUND"]
